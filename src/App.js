import React,{Fragment, useState, useEffect } from 'react';
import Formulario from "./componentes/Formulario";
import Cita from "./componentes/Cita";

function App() {

    //Citas en localStorage
    let citasIniciales = JSON.parse(localStorage.getItem('citas'));
    if(!citasIniciales){
      citasIniciales = []; 
    }
  
  //Crear state de citas - arreglo de citas
  const [citas, guardarCitas] = useState(citasIniciales);

  //UseEffect para realizar ciertas operaciones cuando el state cambia
  /*
  El useEffect siempre sera una funcion de flecha, se ejecuta cuando 
  el componente esta listo y tambien cuando cambia/actualiza por ende 
  se vuelve a recargar, por eso se le pase un arreglo vacio para q no cicle 
  */
  useEffect( ()=> {
    /* 
      JSON.parse() toma una cadena JSON y luego la transforma en un objeto JavaScript. 
      JSON.stringify() toma un objeto JavaScript y luego lo transforma en una cadena JSON.
    */
    let citasIniciales = JSON.parse(localStorage.getItem('citas'));

    if(citasIniciales){
      localStorage.setItem('citas', JSON.stringify(citas));
    }else {
      localStorage.setItem('citas', JSON.stringify([]));
    }
  }, [citas] );


  //Funcion que tome las citas actuales y agregue la nueva
  const crearCita = cita => {
    //console.log(cita);
    guardarCitas([
      ...citas,
      cita
    ]);
  }

  //Funcion que elimina una cita por su id
  const eliminarCita = id => {
    const nuevasCitas = citas.filter(cita => cita.id !== id);
    guardarCitas(nuevasCitas);
  }

  //mensaje condicional
  const titulo = citas.length === 0 
  ? 'No hay citas'
  : 'Administra tus citas'


  return (
    <Fragment>
    <h1>Administrador de Pacientes</h1>
  
    <div className='container'>
      <div className='row'>
        <div className='one-half column'>
          <Formulario
            crearCita = {crearCita}
          />
        </div>
        <div className='one-half column'>
          <h2>{titulo}</h2>
          {citas.map(cita => (
            <Cita
              key = {cita.id}
              cita = {cita}
              eliminarCita = {eliminarCita}
            />
          ))}
        </div>
      </div>
    </div>    
    </Fragment>
  
    );
}

export default App;
