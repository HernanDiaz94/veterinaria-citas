import React,{Fragment, useState} from 'react';
import shortid from 'shortid';
import PropTypes from 'prop-types'; 

const Formulario = ({crearCita}) => {

    //Crear state de citas
    const [cita, actualizarCita] = useState({
        mascota: '',
        propietario:'',
        fecha:'',
        hora:'',
        sintomas:'', 
    });

    const [error, actualizarError] = useState(false);

    //Funcion que se ejecuta cada vez que el usuario escribe en un input
    const actualizarState = e => {
        //console.log(e.target.value);
        actualizarCita({
            ...cita,
            [e.target.name]: e.target.value
        })
    }

    //Extraer los valores

    const {mascota,propietario,fecha,hora,sintomas} = cita;

    //Cuando el usuario presiona agregar Cita
    const submitCita = e => {
        e.preventDefault();
        //console.log('enviando form');

        //Validar 
        if(
            mascota.trim() === ''      
                 || 
            propietario.trim() === ''  
                 ||
            fecha.trim() === ''        
                 || 
            hora.trim() === ''         
                 || 
            sintomas.trim() === ''
        ){
            //console.log('Hay un error');
            actualizarError(true);
            return;
        }

        //Eliminar el mensaje de error
        actualizarError(false);

        //Asignar un ID
        cita.id = shortid.generate();
        //console.log(cita);
        
        //Crear la cita
        crearCita(cita);

        //Reiniciar el form
        actualizarCita({
            mascota: '',
            propietario:'',
            fecha:'',
            hora:'',
            sintomas:'',
        })
    }

    return ( 
            <Fragment>
                <h2>Crear Cita</h2>

                {error ? <p className='alerta-error'>TODOS LOS CAMPOS SON OBLIGATORIOS</p> :null}
                <form
                    onSubmit={submitCita}
                >
                    <label>Nombre mascota</label>
                    <input
                        type ='text'
                        name='mascota'
                        className='u-full-width'
                        placeholder='Nombre mascota'
                        onChange={actualizarState}
                        value={mascota}
                    />

                    <label>Nombre dueño</label>
                    <input
                        type='text'
                        name='propietario'
                        className='u-full-width'
                        placeholder='Nombre dueño de la mascota'
                        onChange={actualizarState}
                        value={propietario}
                    />

                    <label>Fecha</label>
                    <input
                        type='date'
                        name='fecha'
                        className='u-full-width'
                        onChange={actualizarState}
                        value={fecha}
                    />

                    <label>Hora</label>
                    <input
                        type='time'
                        name='hora'
                        className='u-full-width'
                        onChange={actualizarState}
                        value={hora}
                    />

                    <label>Sintomas</label>
                    <textarea
                        className='u-full-width'
                        name='sintomas'
                        onChange={actualizarState}
                        value={sintomas}
                    ></textarea>

                    <button
                        type='submit'
                        className='u-full-width button-primary'
                    >Agregar Cita</button>
                </form>
            </Fragment>
        );
}
 
Formulario.propTypes = {
    crearCita: PropTypes.func.isRequired
}

export default Formulario;